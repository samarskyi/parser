package com.parser;

import com.parser.model.DataModel;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Batman on 12.03.2017.
 */
public class SearchEngine {

    public interface Callback {
        void onAdded(Set<DataModel> data);
    }

    private int page = 1;
    private int maxPage = page;

    private HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    public void getDataList(final Callback callback) {
        if (callback == null) {
            return;
        }
        new Thread(() -> {
            Set<DataModel> dataList = new LinkedHashSet<>();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

            OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(logging)
                    .build();

            do {
                HttpUrl url = new HttpUrl.Builder()
                        .scheme("http")
                        .host("zalog.aval.ua")
                        .addPathSegments("ru/property/search")
                        .addQueryParameter("category_id", "2")
                        .addQueryParameter("type_id", "14")
                        .addQueryParameter("price_currency_id", "1")
                        .addQueryParameter("sort", "price")
                        .addQueryParameter("page", String.valueOf(page))
                        .build();

                Request request = new Request.Builder()
                        .url(url.toString())
                        .build();

                Response response = null;

                try {
                    response = client.newCall(request).execute();
                    if (!response.isSuccessful()) {
                        System.err.println(response.code());
                        return;
                    }

                    String result = response.body().string();

                    Document document = Jsoup.parse(result);

                    if (maxPage == 1) {
                        // getting page count
                        Elements pageCount = document.getElementsByClass("PageCount");
                        System.out.println(pageCount.get(0).text());
                        int pages = Integer.parseInt(pageCount.get(0).text().substring(pageCount.get(0).text().indexOf("/") + 1, pageCount.get(0).text().length() - 1));
                        System.out.println("Page size = " + pages);

                        maxPage = pages;
                    }

                    Elements element = document.getElementsByClass("PropertyDescription");
                    System.out.println("Items on page = " + element.size());

                    int newItems = 0;
                    for (Element resultItem : element) {
                        Elements spans = resultItem.getElementsByTag("span");

                        DataModel dataModel = new DataModel();
                        dataModel.setAddress(spans.get(0).text());
                        dataModel.setSpace(spans.get(1).text());
                        if (spans.size() == 5) {
                            dataModel.setLiveSpace(spans.get(2).text());
                            dataModel.setLevel(spans.get(3).text());
                            dataModel.setPrice(spans.get(4).text());

                            dataList.add(dataModel);
                            newItems++;
                        } else if (spans.size() == 4) {
                            dataModel.setLevel(spans.get(2).text());
                            dataModel.setPrice(spans.get(3).text());

                            dataList.add(dataModel);
                            newItems++;
                        } else if (spans.size() == 3) {
                            dataModel.setPrice(spans.get(2).text());

                            dataList.add(dataModel);
                            newItems++;
                        }
                    }

                    System.out.println("Page " + page + ", added items = " + newItems);

                    callback.onAdded(dataList);
                    dataList.clear();
                } catch (Exception e) {
                    System.err.println("Skipped page + " + page);
                }

                page++;
            } while (page <= maxPage);
        }).start();
    }
}
