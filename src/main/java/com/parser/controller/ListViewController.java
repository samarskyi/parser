package com.parser.controller;

import com.parser.SearchEngine;
import com.parser.model.DataModel;
import com.parser.view.ItemListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by Batman on 12.03.2017.
 */
public class ListViewController implements Initializable, SearchEngine.Callback {

    @FXML
    private ListView<DataModel> listView;

    private SearchEngine searchEngine;
    private Set<DataModel> dataModelSet = new HashSet<DataModel>();
    private ObservableList<DataModel> observableList = FXCollections.observableArrayList();

    public ListViewController() {
        searchEngine = new SearchEngine();
    }

    public void initialize(URL location, ResourceBundle resources) {
        searchEngine.getDataList(this);

        observableList.setAll(dataModelSet);

        listView.setItems(observableList);
        listView.setCellFactory(new Callback<ListView<DataModel>, ListCell<DataModel>>() {
            public ListCell<DataModel> call(ListView<DataModel> param) {
                return new ItemListView();
            }
        });
    }

    public void onAdded(final Set<DataModel> data) {
        observableList.addAll(data);
    }
}