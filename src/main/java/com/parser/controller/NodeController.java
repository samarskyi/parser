package com.parser.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Batman on 12.03.2017.
 */
public class NodeController implements Initializable {

    @FXML
    private Label address;

    @FXML
    private Label price;

    public void setAddress(String t) {
        address.setText(t);
    }

    public void setPrice(String s) {
        price.setText(s);
    }

    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("NodeController initialize");
    }
}
