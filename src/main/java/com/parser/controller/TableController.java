package com.parser.controller;

import com.parser.SearchEngine;
import com.parser.model.DataModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by Batman on 12.03.2017.
 */
public class TableController implements Initializable, SearchEngine.Callback {

    @FXML
    private TableView<DataModel> tableView;

    @FXML
    private TableColumn<DataModel, String> price;

    @FXML
    private TableColumn<DataModel, String> space;

    @FXML
    private TableColumn<DataModel, String> level;

    @FXML
    private TableColumn<DataModel, String> address;

    private SearchEngine searchEngine;
    private Set<DataModel> dataModelSet = new HashSet<>();
    private ObservableList<DataModel> observableList = FXCollections.observableArrayList();

    public TableController() {
        searchEngine = new SearchEngine();
    }

    public void initialize(URL location, ResourceBundle resources) {
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        space.setCellValueFactory(new PropertyValueFactory<>("space"));
        level.setCellValueFactory(new PropertyValueFactory<>("level"));
        address.setCellValueFactory(new PropertyValueFactory<>("address"));

        observableList.setAll(dataModelSet);
        tableView.setItems(observableList);

        searchEngine.getDataList(this);
    }

    public void onAdded(Set<DataModel> data) {
        observableList.addAll(data);
    }
}
