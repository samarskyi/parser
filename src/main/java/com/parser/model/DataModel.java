package com.parser.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Batman on 12.03.2017.
 */
public class DataModel {
    private final SimpleStringProperty price = new SimpleStringProperty();
    private final SimpleStringProperty space = new SimpleStringProperty();
    private final SimpleStringProperty level = new SimpleStringProperty();
    private final SimpleStringProperty number = new SimpleStringProperty();
    private final SimpleStringProperty address = new SimpleStringProperty();
    private final SimpleStringProperty liveSpace = new SimpleStringProperty();

    @Override
    public String toString() {
        return "DataModel{" +
                "price='" + price + '\'' +
                ", space='" + space + '\'' +
                ", level='" + level + '\'' +
                ", number='" + number + '\'' +
                ", address='" + address + '\'' +
                ", liveSpace='" + liveSpace + '\'' +
                '}';
    }

    public String getPrice() {
        return price.get();
    }

    public SimpleStringProperty priceProperty() {
        return price;
    }

    public void setPrice(String price) {
        this.price.set(price);
    }

    public String getSpace() {
        return space.get();
    }

    public SimpleStringProperty spaceProperty() {
        return space;
    }

    public void setSpace(String space) {
        this.space.set(space);
    }

    public String getLevel() {
        return level.get();
    }

    public SimpleStringProperty levelProperty() {
        return level;
    }

    public void setLevel(String level) {
        this.level.set(level);
    }

    public String getNumber() {
        return number.get();
    }

    public SimpleStringProperty numberProperty() {
        return number;
    }

    public void setNumber(String number) {
        this.number.set(number);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getLiveSpace() {
        return liveSpace.get();
    }

    public SimpleStringProperty liveSpaceProperty() {
        return liveSpace;
    }

    public void setLiveSpace(String liveSpace) {
        this.liveSpace.set(liveSpace);
    }
}
