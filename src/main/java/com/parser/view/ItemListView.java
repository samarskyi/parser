package com.parser.view;

import com.parser.model.DataModel;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

import java.io.IOException;

/**
 * Created by Batman on 12.03.2017.
 */
public class ItemListView extends ListCell<DataModel> {

    private Parent itemRoot;

    public ItemListView() {
        try {
            if (itemRoot == null) {
                itemRoot = FXMLLoader.load(getClass().getResource("/item.fxml"));
            }
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public void updateItem(DataModel model, boolean empty) {
        super.updateItem(model, empty);

        Label label_AppName = (Label) itemRoot.lookup("#address");
        if (model != null) {
            label_AppName.setText(model.getAddress());
            Platform.runLater(new Runnable() {
                public void run() {
                    setGraphic(itemRoot);
                }
            });
        }
    }
}