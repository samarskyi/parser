package com.parser.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Created by Batman on 12.03.2017.
 */
public class Main extends Application {

    private static final float SCALE = 0.75f;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));

        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(root, bounds.getWidth() * SCALE, bounds.getHeight() * SCALE);

        stage.setScene(scene);
        stage.show();
    }
}
